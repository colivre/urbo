class OfficialDiaryMailer < ActionMailer::Base

  helper ApplicationHelper

  def notification_email(official_diary, subscriber)
    @official_diary = official_diary
    config = OfficialDiaryConfiguration.current
    @hostname = config.hostname
    mail(
      to: subscriber.email,
      from: config.mail_from,
      subject: config.mail_subject,
    )
  end
end
