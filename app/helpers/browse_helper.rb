# encoding: UTF-8

module BrowseHelper

  # FIXME should use locale data instead!

  MONTHS = %w[Janeiro Fevereiro Março Abril Maio Junho Julho Agosto Setembro Outubro Novembro Dezembro]
  ABBREV = MONTHS.map { |m| m[0..2] }

  def period_title(period)
    _("%s de %d") % [MONTHS[period.month - 1], period.year]
  end

  def month_selector_data(period)
    (1..12).select do |month|
      Date.new(period.year, month, 1) <= Date.today
    end.map do |month|
      [ABBREV[month-1], month, month == period.month && :active || nil]
    end
  end

  def month_abbrev(n)
    ABBREV[n-1]
  end

  def subject_list(subjects)
    content_tag(:ul) do
      subjects.map { |s| content_tag :li, link_to(s.title, subject_download_path(s)) }.join.html_safe
    end
  end

end
