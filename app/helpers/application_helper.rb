# encoding: UTF-8

module ApplicationHelper

  def tenant_name
    # FIXME
    @tenant_name ||= 'Prefeitura de Vitória da Conquista'
  end

  def site_title
    [tenant_name, 'Diário oficial'].join(' — ')
  end

  def download_path(official_diary)
    date = official_diary.date
    # FIXME hardcoding route to be able to use this helper from Typus
    "/#{date.year}/#{date.month}/#{date.day}.pdf"
  end

  def subject_download_path(subject)
    download_path(subject.official_diary) + (subject.page.present? && ('#page=%d' % subject.page) || '')
  end

  def official_diary_title(official_diary)
    [site_title, "Edição #{official_diary.number}, #{l official_diary.date, :format => :long}"].join(' — ')
  end

end
