class Book < ActiveRecord::Base
  attr_accessible :name, :position
  validates_presence_of :name
  validates_uniqueness_of :name
end
