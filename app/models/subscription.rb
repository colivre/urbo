require 'digest/sha1'

class Subscription < ActiveRecord::Base
  attr_accessible :email, :enabled
  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  validates_uniqueness_of :email

  scope :enabled, where(enabled: true)

  before_create do |s|
    s.new_token
  end

  after_create do |s|
    SubscriptionMailer.confirm_subscription(s).deliver
  end

  def self.for(params)
    if params[:email]
      s = self.find_by_email(params[:email])
      if s
        s
      else
        self.new(params)
      end
    else
      self.new
    end
  end

  def new_token
    self.token = SecureRandom.hex(10)
    self
  end

  def new_token!
    self.new_token.save!
  end

  def cancel!
    self.enabled = false
    self.save!
  end

  def confirm!
    self.enabled = true
    self.save!
  end
end
