# encoding: UTF-8

class OfficialDiary < ActiveRecord::Base

  YEAR_ZERO = Rails.configuration.database_configuration[Rails.env]['year_zero']

  attr_accessible :date, :number
  validates_presence_of :date, :number
  validates_uniqueness_of :date, :number
  has_many :subjects
  has_one :attachment

  def initialize(*args)
    super(*args)
    self.number ||= (OfficialDiary.maximum(:number) || 0) + 1
  end

  # XXX IMPORTANT: the implementations of the `published` scope and the
  # `published?` method must be in sync with each other
  scope :published, -> { where ['publication_date <= ? AND EXISTS(SELECT 1 FROM attachments WHERE attachments.official_diary_id = official_diaries.id)', Time.now] }

  def published?
    publication_date && publication_date <= Time.now && self.attachment.present?
  end

  class AttchachmentRequired < Exception; end

  def publish!
    raise AttchachmentRequired.new if !attachment.present?
    self.publication_date = Time.now
    self.save!
    self
  end

  def published_status
    published? && _('Publicado') || _('Não publicado')
  end

  def self.browse(period)
    if period.range.first > Date.today
      raise ArgumentError, "cannot browse official diaries in the future"
    end

    published.where(:date => period.range).order('date DESC')
  end

  def self.all_years
    @all_years ||= OfficialDiary.connection.select_values('select distinct(extract(year from date)) as year from official_diaries order by year desc')
  end
  class << self
    attr_writer :all_years
  end

  def year
    year_zero = (YEAR_ZERO || self.class.all_years.min).to_i
    1 + date.year - year_zero
  end

  scope :not_notified, -> { where('notification_sent_at IS NULL') }

end
