# encoding: UTF-8

require 'tmpdir'

class Attachment < ActiveRecord::Base

  belongs_to :official_diary

  validates_presence_of :official_diary_id

  attr_accessible :official_diary_id, :file_data, :generate, :publish

  attr_reader :file_data

  def file_data=(file)
    self.data = file.read
    @file_data = file
    @file_data
  end

  attr_accessor :generate
  attr_accessor :publish

  validates_presence_of :file_data, if: ->(a) { a.publish }
  validate :require_valid_signature_when_publishing
  def require_valid_signature_when_publishing
    if self.publish && self.file_data && !self.valid_signature?
      self.errors.add(:file_data, _('não possui uma assinatura digital válida'))
    end
  end

  # XXX This might become a performance issue as the PDF is not being generated
  # in a background worker process. Right now it is not a problem since the PDF
  # is being generated in the admin interface only, and this will probably only
  # be allowed to a few users.
  #
  before_save do |attachment|
    if attachment.official_diary && attachment.generate
      pdf = OfficialDiaryPdf.new(attachment.official_diary)
      attachment.file_data = pdf.generate!
    end
  end

  after_save do |attachment|
    if attachment.publish
      attachment.official_diary.publish!
    end
  end

  def valid_signature?
    tempfile = Tempfile.new('pdf', encoding: self.data.encoding)
    tempfile.write(self.data)
    tempfile.flush

    options = {
      verbosity: Origami::Parser::VERBOSE_QUIET,
    }
    pdf = Origami::PDF.read(tempfile.path, options)

    certificates = []
    config = OfficialDiaryConfiguration.current
    if config.certificate.present?
      certificates << OpenSSL::X509::Certificate.new(config.certificate)
    end

    begin
      result = pdf.verify(trusted: certificates)
      result
    rescue Origami::PDF::SignatureError
      false
    ensure
      tempfile.unlink
    end
  end

end
