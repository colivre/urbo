# encoding: UTF-8

require 'tmpdir'

class OfficialDiaryConfiguration < ActiveRecord::Base
  attr_accessible :number_of_columns, :font_size, :header, :footer, :preamble
  attr_accessible :cover_file, :wallpaper_file
  attr_accessible :certificate
  attr_accessible :hostname, :protocol, :mail_from, :mail_subject

  class JpgValidator < ActiveModel::EachValidator

    def validate_each(record, attribute, value)
      return if value.blank?

      Dir.mktmpdir do |dir|

        f = File.open(File.join(dir, 'image'), 'wb')
        f.write(value)
        f.close

        io = IO.popen(['file', '--brief', '--mime-type', f.path])
        mime_type = io.read.strip
        io.close

        if mime_type != 'image/jpeg'
          record.errors.add(
            attribute,
            options[:message] || _('não é um arquivo JPG')
          )
        end
      end
    end
  end
  validates :cover, :wallpaper, jpg: true

  def self.number_of_columns
    [1, 2, 3]
  end

  def self.font_sizes
    [10, 12, 14]
  end

  def self.protocols
    %w[http https]
  end

  def initialize(*args)
    super(*args)
    last = OfficialDiaryConfiguration.last
    if last
      self.number_of_columns ||= last.number_of_columns
      self.font_size         ||= last.font_size
      self.header            ||= last.header
      self.footer            ||= last.footer
      self.preamble          ||= last.preamble
    end
  end

  def self.current
    self.last || self.new
  end

  attr_reader :cover_file
  def cover_file=(file)
    self.cover = file.read
  end

  attr_reader :wallpaper_file
  def wallpaper_file=(file)
    self.wallpaper = file.read
  end
end
