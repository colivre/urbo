# encoding: UTF-8

require 'digest'
require 'timeout'

class ExtraFile < ActiveRecord::Base
  attr_accessible :upload, :title

  belongs_to :subject

  validates_presence_of :data, :title, :filename

  attr_reader :upload
  def upload=(file)
    if file.respond_to?(:content_type)
      self.content_type = file.content_type
    end
    if file.respond_to?(:original_filename)
      self.filename = File.basename(file.original_filename)
    end
    self.data = file.read
  end

  def convert_to_pdf(output=nil)
    output ||= self.export_filename

    if self.content_type == 'application/pdf'
      File.open(output, 'wb') do |f|
        f.write(self.data)
      end
      return
    end

    outdir = File.dirname(output)
    input = File.join(outdir, self.filename)
    File.open(input, 'wb') do |f|
      f.write(self.data)
    end

    pdf = input.sub(/\.[^.]+$/, '.pdf')
    IO.popen(conversion_command(input, outdir)) do |conversion|
      begin
        Timeout.timeout(conversion_timeout) do
          while !File.exist?(pdf)
            sleep 0.1
          end

          # will block until the process finishes. If the resulting PDF
          # did not show up after the timeout, something already went
          # very wrong and we shouldn't even reach this point.
          conversion.close

          if $?.success?
            FileUtils.mv(pdf, output)
            FileUtils.rm_f(input)
          else
            raise ConversionToPdfFailed.new(self.filename)
          end
        end
      rescue Timeout::Error => ex
        Process.kill 'TERM', conversion.pid
        conversion.close
        raise ConversionToPdfFailed.new(self.filename)
      end
    end
  end

  def conversion_command(input, outdir)
    [
      'libreoffice',
      '--headless',
      '--convert-to', 'pdf',
      '--outdir', outdir,
      input
    ]
  end

  def conversion_timeout
    if Rails.env.development?
      5 # seconds
    else
      30 # seconds
    end
  end

  class ConversionToPdfFailed < Exception
    def initialize(filename)
      super(_("Não foi possível converter [%s] para PDF" % filename))
    end
  end

  def label
    subject && id && "#{subject.id}-#{self.id}" || Digest::SHA1.hexdigest(self.data)
  end

  def export_filename
    "#{label}.pdf"
  end

end
