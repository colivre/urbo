class OfficialDiaryNotification

  def self.send_all_pending
    OfficialDiary.published.not_notified.each do |official_diary|
      Subscription.enabled.find_each do |subscriber|
        OfficialDiaryMailer.notification_email(official_diary, subscriber).deliver
      end
      official_diary.notification_sent_at = Time.now
      official_diary.save!
    end
  end

end
