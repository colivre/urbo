require 'fileutils'
require 'erb'

class OfficialDiaryPdf

  attr_reader :official_diary, :config

  def initialize(official_diary)
    @official_diary = official_diary
    @config = OfficialDiaryConfiguration.current
  end

  def template
    @template ||= read_template(:official_diary)
  end

  def latex_source
    ERB.new(template).result(binding)
  end

  def remove_paragraph_breaks(text)
    text.split("\n").select { |line| !line.blank? }.join("\n\\\\\n")
  end

  def weekday
    I18n.t(:"date.day_names")[official_diary.date.wday] + ((1..5).include?(official_diary.date.wday) && '-feira' || '')
  end

  def date
    '%d de %s de %d' % [
      official_diary.date.day,
      BrowseHelper::MONTHS[official_diary.date.month - 1],
      official_diary.date.year
    ]
  end

  class GenerationFailure < Exception; end

  def generate!
    Dir.mktmpdir do |dir|
      filename = File.join(dir, official_diary.id.to_i.to_s + '.pdf')
      create!(filename)
      begin
        File.open(filename)
      rescue
        msg = _('Houve um erro ao gerar o PDF.')

        log = Dir.glob("#{dir}/*.log").first
        log = log && ("\n\n" + File.read(filename.sub(/\.pdf$/, '.log'))) || ''

        raise GenerationFailure.new(msg + log)
      end
    end
  end

  def create!(filename)
    source = filename.sub(/\.pdf$/, '.tex')
    File.open(source, 'w') do |f|
      f.write(latex_source)
    end
    Dir.chdir(File.dirname(source)) do
      export_cover
      export_wallpaper
      export_extra_files
      run_latex(File.basename(source))
    end
  end

  def self.filters
    @filters ||= []
  end

  def self.filter(&block)
    filters << block
  end

  def filter!(text)
    self.class.filters.each do |filter|
      filter.call(text)
    end
  end

  # Make tables work in 2-column mode
  filter do |text|
    text.gsub!('{longtable}', '{supertabular}')
  end

  # Avoid very long words (such as in URLs) overflowing the margin
  #
  # Based on data from the packages wamerican and wbrazilian (which provide
  # /usr/share/dict/{american-english,brazilian}) from Debian GNU/Linux 8, the
  # maximum word length in Brazilian Portuguese and American English is 24.
  # Thus, we mark any works longer than 24 with \seqsplit to make them be
  # automatically split by LaTeX.
  filter do |text|
    text.gsub!(/[[:alnum:]]{25,}/, '\seqsplit{\&}')
  end

  # Mark URLs with \seqsplit since they are usually long enough
  filter do |text|
    text.gsub!(/(https?:\/\/|www\.)[\S}]*/) do
      url = $&.gsub(%r{[^a-zA-Z0-9./:-]}, '{\&}').gsub('/', '\slash{}')
      '\seqsplit{' + url + '}'
    end
  end

  # Replace / with \slash\hspace{0pt} to allow proper hyphenation
  filter do |text|
    text.gsub!(/([[:alnum:]]+)\/([[:alnum:]]+)/, '\1\slash\hspace{0pt}\2')
  end

  def html_to_latex(contents)
    source = IO.popen(['pandoc', '--from', 'html', '--to', 'latex'], 'w+') do |io|
      io.write(contents)
      io.close_write
      io.read
    end.strip
    filter!(source)
    source
  end

  private

  def read_template(name)
    template_dir = File.join(File.dirname(__FILE__), '../views/pdf')
    template_file = File.join(template_dir, name.to_s + '.tex.erb')
    File.read(template_file)
  end

  def export_cover
    if config.cover.present?
      File.open('cover.jpg', 'wb') do |f|
        f.write(config.cover)
      end
    end
  end

  def export_wallpaper
    if config.wallpaper.present?
      File.open('wallpaper.jpg', 'wb') do |f|
        f.write(config.wallpaper)
      end
    end
  end

  def export_extra_files
    official_diary.subjects.each do |subject|
      subject.extra_files.each do |f|
        f.convert_to_pdf
      end
    end
  end

  def run_latex(texfile)
    # Needs to be run twice for \tableofcontents
    2.times do
      system('pdflatex %s >/dev/null 2>&1 </dev/null' % texfile)
    end
  end

end
