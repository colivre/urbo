class OfficialDiaryPublication

  def initialize(official_diary, signed_pdf)
    @official_diary = official_diary
    @signed_pdf = signed_pdf
  end

  def perform
    verify_signature
    replace_document
    publish_official_diary
  end

  protected

  def verify_signature
  end

  def replace_document
  end

  def publish_official_diary
  end

end
