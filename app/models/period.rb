class Period

  attr_reader :year, :month

  def initialize(year=nil, month=nil, today=Date.today)
    if year.nil? && month.nil?
      @year = today.year
      @month = today.month
    elsif year.nil? || month.nil?
      raise ArgumentError, 'Period requires both year and month'
    else
      @year = year.to_i
      @month = month.to_i
      if @month < 1 || @month > 12
        raise ArgumentError, 'invalid month'
      end
    end
  end

  def to_a
    [year, month]
  end

  def range
    start = Date.new(year, month, 1)
    finish = start + 1.month - 1.day
    start..finish
  end

end

def Period(*input)
  if input.first.is_a?(Period)
    input.first
  else
    Period.new(*input)
  end
end
