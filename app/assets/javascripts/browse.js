// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(function() {
  $('[rel=popover]').popover();
  $('.popover-title button.close').live('click', function() {
    $(this).closest('.toc').find('[rel=popover]').popover('toggle');
  });
})
