class Admin::AttachmentsController < Admin::ResourcesController
  before_filter :check_published, :only => [:edit, :update, :destroy]

  rescue_from ExtraFile::ConversionToPdfFailed, with: :pdf_generation_failed
  rescue_from OfficialDiaryPdf::GenerationFailure, with: :pdf_generation_failed

  protected

  def pdf_generation_failed(exception)
    @exception = exception
    render action: 'exception'
  end

  private

  def check_published
    if @item.official_diary.published? && admin_user.role != 'admin'
      render 'shared/access_denied', :status => :forbidden
    end
  end
end
