class Admin::OfficialDiariesController < Admin::ResourcesController
  before_filter :check_published, :only => [:edit, :update, :destroy]

  def source
    diary = OfficialDiary.find(params[:id])
    pdf = OfficialDiaryPdf.new(diary)
    render text: pdf.latex_source, content_type: 'text/plain'
  end

  private

  def check_published
    if @item.published? && admin_user.role != 'admin'
      render 'shared/access_denied', :status => :forbidden
    end
  end
end
