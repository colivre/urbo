class Admin::SubjectsController < Admin::ResourcesController
  before_filter :check_published, :only => [:edit, :update, :destroy]
  before_filter :set_separtment, :only => [:new]

  private

  def check_published
    if @item.published? && admin_user.role != 'admin'
      render 'shared/access_denied', :status => :forbidden
    end
  end

  def set_separtment
    params[:resource][:department_id] ||= admin_user.department_id
  end

end
