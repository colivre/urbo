# encoding: UTF-8

class BrowseController < ApplicationController

  include ApplicationHelper

  def index
    # this is a separate action *just* for a custom template
    month
  end

  def month
    @period = Period.new(params[:year], params[:month])
    @official_diaries = OfficialDiary.browse(@period)
  rescue ArgumentError => e
    not_found
  end

  def download
    year = Integer(params[:year])
    month = Integer(params[:month])
    day = Integer(params[:day])

    date = Date.new(year, month, day)
    official_diary = OfficialDiary.find_by_date(date)

    attachment = official_diary.attachment

    if attachment
      filename = official_diary_title(official_diary) + ".pdf"
      send_data attachment.data, type: 'application/pdf', filename: filename, disposition: 'inline'
      expires_in 1.day
    else
      not_found
    end
  end

  private

  def not_found
    render :text => ('Página não encontrada'), :status => :not_found
  end

end
