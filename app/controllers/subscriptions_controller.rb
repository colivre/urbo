# encoding: UTF-8

class SubscriptionsController < ApplicationController

  def index
    redirect_to action: 'new'
  end

  def new
    @subscription = Subscription.new
  end

  def create
    @subscription = Subscription.for(params[:subscription])
    if @subscription.save
      flash.notice = _('Verifique o seu email para confirmar a sua inscrição.')
      redirect_to action: 'new'
    else
      render action: 'new'
    end
  end

  def update
    @subscription = Subscription.find_by_token(params[:id])
    @subscription.confirm!
    render action: 'confirm'
  end

  def show
    @subscription = Subscription.find_by_token(params[:id])
    unless @subscription
      redirect_to action: 'new'
    end
  end

  def destroy
    @subscription = Subscription.find_by_token(params[:id])
    if @subscription
      @subscription.cancel!
      flash.notice = _('Sua inscrição foi cancelada com sucesso.')
    end
    redirect_to action: 'new'
  end

end
