module Typus
  module Authentication
    module Custom

      include Base

      def admin_user
        @admin_user ||= User.find(session[:user_id]) if session[:user_id]
      rescue ActiveRecord::RecordNotFound
        nil
      end

      def authenticate
        unless admin_user
          redirect_to login_url
        end
      end
    end
  end
end
