require_relative "../test_helper"

class RoutingTest < ActionDispatch::IntegrationTest

  test 'root' do
    assert_routing '', :controller => 'browse', :action => 'index'
  end

  test 'browse by year and month' do
    assert_routing '2013/10', :controller => 'browse', :action => 'month', :year => '2013', :month => '10'
  end

  test 'search' do
    assert_routing 'search', :controller => 'search', :action => 'index'
  end
end
