require_relative "../test_helper"

class SessionsControllerTest < ActionController::TestCase

  test "should get new" do
    get :new
    assert_response :success
  end

  test 'login' do
    post :create, username: 'joao', password: 'test'
    assert_equal users(:joao).id, session[:user_id]
  end

  test 'failed login' do
    post :create, username: 'joao', password: 'wrongpasword'
    assert_nil session[:user_id]
  end

  test 'logout' do
    post :create, username: 'joao', password: 'test'
    get :destroy
    assert_nil session[:user_id]
  end

end
