require_relative '../test_helper'

class SubscriptionMailerTest < ActionMailer::TestCase
  test 'it works' do
    s = Subscription.create!(email: 'foo@example.com')
    assert_mail_sent(1) do
      SubscriptionMailer.confirm_subscription(s).deliver
    end
  end
end
