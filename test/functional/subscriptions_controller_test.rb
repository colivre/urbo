require_relative "../test_helper"

class SubscriptionsControllerTest < ActionController::TestCase
  test "new subscription" do
    assert_difference 'Subscription.count', 1 do
      post :create, subscription: { email: 'foo@example.com'}
    end
  end

  test 'does not duplicate subscriptions' do
    assert_difference 'Subscription.count', 1 do
      post :create, subscription: { email: 'foo@example.com'}
      post :create, subscription: { email: 'foo@example.com'}
    end
  end

  test 'cancels subscriptions' do
    s = Subscription.create(email: 'foo@bar.com', enabled: true)
    assert_difference 'Subscription.where(enabled: true).count', -1 do
      delete :destroy, id: s.token
    end
  end

  test 'confirms subscription' do
    s = Subscription.create!(email: 'foo@bar.com')
    put :update, id: s.token
    s.reload
    assert s.enabled
  end

end
