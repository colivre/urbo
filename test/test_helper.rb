ENV["RAILS_ENV"] = "test"
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'minitest/mock'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.(yml|csv) for all tests in alphabetical order.
  #
  # Note: You'll currently still have to declare fixtures explicitly in integration tests
  # -- they do not yet inherit this setting
  fixtures :all

  # Add more helper methods to be used by all tests here...

  include ActionDispatch::TestProcess

  def pdf
    fixture_file_upload('files/test.pdf', 'application/pdf')
  end

  def signed_pdf
    fixture_file_upload('files/test_signed.pdf', 'application/pdf')
  end

  def invalid_signed_pdf
    fixture_file_upload('files/test_signed_invalid.pdf', 'application/pdf')
  end

  module WithPDF

    def publish!
      attachment = Attachment.new
      attachment.official_diary_id = self.id
      attachment.file_data = File.open(Rails.root.join('test/fixtures/files/test.pdf'))
      attachment.save!
      super
      self
    end

  end

  def new_official_diary(attrs)
    if !attrs.has_key?(:date)
      if @autoinc_date
        @autoinc_date = @autoinc_date + 1.day
      else
        @autoinc_date = Date.today
      end
      attrs[:date] = @autoinc_date
    end
    OfficialDiary.new(attrs)
  end

  def create_official_diary(attrs={})
    new_official_diary(attrs).tap do |diary|
      diary.save!
      diary.extend(WithPDF)
    end
  end

  def mime_type(file)
    io = IO.popen(['file', '--brief', '--mime-type', file])
    mt = io.read.strip
    io.close
    mt
  end

  def assert_mail_sent(n)
    assert_difference 'ActionMailer::Base.deliveries.size', n do
      yield
    end
  end
end
