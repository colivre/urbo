# encoding: UTF-8

require_relative "../test_helper"

class SubjectTest < ActiveSupport::TestCase

  test 'mandatory fields' do
    subject = Subject.new
    assert(!subject.valid?)
    assert subject.errors[:book_id].any?
    assert subject.errors[:official_diary_id].any?
  end

  test 'inherit date from official diary' do
    diary = OfficialDiary.new(:date => Date.new(2013,1,1))
    subject = Subject.new
    subject.official_diary = diary
    subject.valid?

    assert_equal diary.date, subject.date
  end

  test 'inherit number from official diary' do
    diary = OfficialDiary.new(number: 9999)
    subject = Subject.new
    subject.official_diary = diary
    subject.valid?
    assert_equal 9999, subject.number
  end

  test 'mass assign page' do
    Subject.new(page: 1)
  end

  test 'normalize numbers in search' do
    o = create_official_diary
    s = Subject.create!(title: 'foo', contents: '077', book_id: 1, official_diary_id: o.id)
    assert Subject.search('77').include?(s)
  end

  test 'split words in search' do
    o = create_official_diary
    s = Subject.create!(title: 'bar', contents: '003/2014', book_id: 1, official_diary_id: o.id)
    assert Subject.search('003').include?(s)
    assert Subject.search('2014').include?(s)
  end

  test 'ignore accents on search' do
    o = create_official_diary
    s = Subject.create!(title: 'foo', contents: 'Diário oficial', book_id: 1, official_diary_id: o.id)

    assert Subject.search('diario').include?(s)
  end

  test 'inside HTML' do
    o = create_official_diary
    s = Subject.create!(title: 'foo', contents: '<p><strong>interesting</strong></p>', book_id: 1, official_diary_id: o.id)

    assert Subject.search('interesting').include?(s)
  end

  test 'allow no content with extra files' do
    subject = Subject.new
    subject.valid?
    assert subject.errors[:contents].any?

    subject.extra_files << ExtraFile.new
    subject.valid?
    assert !subject.errors[:contents].any?
  end

end
