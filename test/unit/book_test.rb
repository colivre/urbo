require_relative "../test_helper"

class BookTest < ActiveSupport::TestCase
  test 'mandatory fields' do
    book = Book.new
    assert(!book.valid?)
    assert book.errors[:name].any?
  end
end
