require_relative "../test_helper"

class AttachmentTest < ActiveSupport::TestCase

  setup do
    OfficialDiaryConfiguration.create!(
      certificate: fixture_file_upload('files/ssl-cert-snakeoil.pem').read
    )
  end

  test 'save binary data' do
    attachment = Attachment.new
    attachment.file_data = pdf

    assert_equal pdf.read, attachment.data
  end

  test 'does not generate PDF by default' do
    attachment = create!
    assert !attachment.file_data.present?
  end

  test 'generate PDF when generate is true' do
    attachment = create!(
      official_diary_id: create_official_diary.id,
      generate: true,
    )

    assert attachment.file_data.present?
  end

  test 'unsigned PDF' do
    attachment = Attachment.new
    attachment.file_data = pdf
    assert_equal false, attachment.valid_signature?
  end

  test 'signed PDF' do
    attachment = Attachment.new
    attachment.file_data = signed_pdf
    assert_equal true, attachment.valid_signature?
  end

  test 'PDF signed with wrong private key' do
    attachment = Attachment.new
    attachment.file_data = invalid_signed_pdf
    assert_equal false, attachment.valid_signature?
  end

  test 'require an upload when publishing' do
    attachment = create!
    attachment.publish = true
    assert !attachment.valid?
    attachment.file_data = signed_pdf
    assert attachment.valid?
  end

  test 'require valid signature when publishing' do
    attachment = create!

    attachment.publish = true
    attachment.file_data = invalid_signed_pdf
    assert !attachment.valid?, "does not accept file with invalid signature"

    attachment.file_data = signed_pdf
    assert attachment.valid?, "accepts file with valid signature"
  end

  test 'publish diary when saved with a valid PDF' do
    a = create!(official_diary_id: create_official_diary.id)
    a.publish = true
    a.file_data = signed_pdf
    a.save!

    assert a.official_diary.published?, 'must publish official diary when a valid PDF is uploaded'
  end

  private

  def create!(attrs={})
    a = Attachment.new(attrs)
    a.official_diary_id ||= 1
    a.save!
    a
  end

end
