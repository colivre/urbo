# encoding: UTF-8

require_relative "../test_helper"

class SearchTest < ActiveSupport::TestCase

  test 'creation with hash' do
    search = Search.new query: 'foo'
    assert_equal 'foo', search.query
  end

  test 'preload' do
    assert Search.new.preload.is_a?(Enumerable), 'preload must be an Enumerable'
  end

  test 'normalize numbers: remove leading zero' do
    assert_equal "3 003", Search.normalize('003')
  end

  test 'normalize numbers: split on non-words' do
    assert_match "003 2014", Search.normalize('003/2014')
  end

  test 'normalize' do
    assert_equal "Licitação número 3 003 2014 003/2014", Search.normalize("Licitação número 003/2014")
  end

  test 'normalize thousands' do
    assert_equal '12345 12.345 2013 12.345/2013', Search.normalize('12.345/2013')
  end

  test 'not search if query is blank; use only filters' do
    d = create_official_diary.publish!
    d.subjects << Subject.new(title: 'subj 1').tap { |s| s.book = Book.first; s.contents = 'lorem ipsum' }
    d.subjects << Subject.new(title: 'subj 2').tap { |s| s.book = Book.first; s.contents = 'lorem ipsum' }
    d.subjects.each(&:save!)

    s = Search.new(query: '', date: d.date)
    assert_equal 2, s.results.count
  end

  test 'declare itself as blank only if all fields are blank' do
    s = Search.new
    assert s.blank?
    ([:query] + s.filters).each do |f|
      s.send("#{f}=", 'something')
      assert !s.blank?
      s.send("#{f}=", nil)
    end
  end

end
