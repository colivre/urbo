require_relative "../test_helper"
require 'minitest/mock'

class OfficialDiaryTest < ActiveSupport::TestCase

  setup do
    @diary = OfficialDiary.new
  end

  test 'date is mandatory' do
    @diary.valid?
    assert @diary.errors[:date].any?
  end

  test 'number is mandatory' do
    @diary.number = nil
    @diary.valid?
    assert @diary.errors[:number].any?
  end

  test 'browse by period' do
    d1 = create!(date: Date.new(2013,1,1)).publish!
    d2 = create!(date: Date.new(2013,2,1)).publish!
    assert_equal [d1], OfficialDiary.browse(Period(2013,1)).to_a
  end

  test 'cannot query the future' do
    Date.stub :today, Date.new(2013,7,1) do
      assert_raise ArgumentError do
        OfficialDiary.browse(Period(2013,8))
      end
    end
  end

  test 'year' do
    OfficialDiary.all_years = nil
    create!(:date => Date.today - 5.years)
    assert_equal 6, new(:date => Date.today).year
  end

  test 'published' do
    published = create!.publish!
    unpublished = create!

    assert_equal [published], OfficialDiary.published.to_a
  end

  test 'unpublished without a publication date'do
    assert !OfficialDiary.new.published?
  end

  test 'cannot publish without attachment' do
    diary = OfficialDiary.new
    diary.date = Date.today
    diary.save!

    assert_raise OfficialDiary::AttchachmentRequired do
      diary.publish!
    end

  end

  test 'browse only published items' do
    published = create!(:date => Date.new(2013,07,15)).publish!
    unpublished = create!(:date => Date.new(2013,07,20))
    assert_equal [published], OfficialDiary.browse(Period(2013,7)).to_a
  end

  test 'non-notified' do
    OfficialDiary.delete_all
    past = create!
    past.notification_sent_at = Time.now
    past.save!

    future = create!

    assert_equal [future], OfficialDiary.not_notified
  end

  private

  def new(attrs = {})
    new_official_diary(attrs)
  end

  def create!(attrs = {})
    create_official_diary(attrs)
  end

end
