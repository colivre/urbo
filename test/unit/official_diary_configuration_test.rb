require_relative "../test_helper"

class OfficialDiaryConfigurationTest < ActiveSupport::TestCase

  test 'inherit values from last configuration saved' do
    OfficialDiaryConfiguration.create!(number_of_columns: 1, font_size: 12)

    c = OfficialDiaryConfiguration.new
    assert_equal 1, c.number_of_columns
    assert_equal 12, c.font_size
  end

  test 'provide the current configuration when no configuration was saved' do
    OfficialDiaryConfiguration.delete_all
    assert_kind_of OfficialDiaryConfiguration, OfficialDiaryConfiguration.current
  end

  test 'return last saved item as current' do
    c = OfficialDiaryConfiguration.create!
    assert_equal c, OfficialDiaryConfiguration.current
  end

  test 'validate that cover is a JPG' do
    c = OfficialDiaryConfiguration.new
    c.cover = '123'
    c.valid?

    assert c.errors[:cover].size > 0

    c.cover = File.read(fixture_file_upload('files/white.jpg'), mode: 'rb')
    c.valid?
    assert c.errors[:cover].size == 0
  end

end
