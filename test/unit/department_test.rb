require_relative "../test_helper"

class DepartmentTest < ActiveSupport::TestCase
  test 'has name' do
    assert Department.new.respond_to?(:name)
    assert Department.new.respond_to?(:name=)
  end
end
