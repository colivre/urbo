namespace :app do
  task :default_admin => :environment do
    unless User.find_by_username('admin')
      user = User.new
      user.role = 'admin'
      user.username = 'admin'
      user.password = user.password_confirmation = 'admin'
      user.email = 'admin@example.com'
      user.save!
    end
  end
end
