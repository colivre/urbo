Urbo::Application.routes.draw do
  root :to => 'browse#index'
  match ':year/:month' => 'browse#month', :year => /\d+/, :month => /\d+/, :as => :period
  match ':year/:month/:day.:format' => 'browse#download', :year => /\d+/, :month => /\d+/, :day => /\d+/
  get "search", :controller => 'search', :action => 'index'

  delete 'logout' => 'sessions#destroy', :as => 'destroy_admin_session'
  get 'login' => 'sessions#new'
  resources :sessions

  resources :subscriptions
end
