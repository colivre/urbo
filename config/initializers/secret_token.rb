# Be sure to restart your server when you modify this file.

# Your secret key for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!
# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
secret_token_file = File.join(Rails.root, 'tmp', 'secret_token.txt')
if !File.exists?(secret_token_file)
  FileUtils.mkdir_p(File.dirname(secret_token_file))
  require 'securerandom'
  random_string = SecureRandom.hex(128)
  File.open(secret_token_file, 'w') do |f|
    f.write(random_string)
  end
end
Urbo::Application.config.secret_token = File.read(secret_token_file)
