class AddPageToSubjects < ActiveRecord::Migration
  def change
    add_column :subjects, :page, :integer
  end
end
