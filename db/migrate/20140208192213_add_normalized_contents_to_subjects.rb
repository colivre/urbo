class AddNormalizedContentsToSubjects < ActiveRecord::Migration
  def change
    add_column :subjects, :normalized_contents, :text
    execute "DROP INDEX subjects_contents"
    Subject.find_each do |subject|
      subject.save! # will force populating normalized_contents
    end
    execute "CREATE INDEX subjects_normalized_contents ON subjects USING gin(to_tsvector('urbo_portuguese', normalized_contents));"
  end
end
