class CreateOfficialDiaries < ActiveRecord::Migration
  def change
    create_table :official_diaries do |t|
      t.date    :date,    :null => false
      t.integer :number,  :null => false

      t.timestamps
    end
  end
end
