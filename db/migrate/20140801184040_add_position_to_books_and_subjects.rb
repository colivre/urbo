class AddPositionToBooksAndSubjects < ActiveRecord::Migration
  def up
    add_column :books, :position, :integer, default: 0
    add_column :subjects, :position, :integer, default: 0
  end

  def down
    remove_column :books, :position
    remove_column :subjects, :position
  end
end
