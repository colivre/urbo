class AddRoleToUser < ActiveRecord::Migration
  def change
    add_column :users, :role, :string, :default => 'editor'
    execute "UPDATE users set role = 'admin' WHERE username = 'admin'"
    execute "UPDATE users set role = 'editor' WHERE username != 'admin'"
  end
end
