class ChangeSubcriptionsEnabledToFalseAsDefault < ActiveRecord::Migration
  def up
    change_column :subscriptions, :enabled, :boolean, :default => false
  end

  def down
    change_column :subscriptions, :enabled, :boolean, :default => true
  end
end
