class FullTextSearchSetup < ActiveRecord::Migration
  def up
    # FIXME for `CREATE EXTENSION` to work the user running the migration has
    # to be a superuser!
    execute 'CREATE EXTENSION unaccent;'
    execute "CREATE TEXT SEARCH CONFIGURATION urbo_portuguese (copy=portuguese);"
    execute "ALTER TEXT SEARCH CONFIGURATION urbo_portuguese ALTER MAPPING FOR asciiword, word WITH unaccent, portuguese_stem;"
    execute "CREATE INDEX subjects_contents ON subjects USING gin(to_tsvector('urbo_portuguese', contents));"
    execute "CREATE INDEX attachment_text_data ON attachments USING gin(to_tsvector('urbo_portuguese', text_data));"
  end

  def down
    execute "DROP INDEX attachment_text_data"
    execute "DROP INDEX subjects_contents"
    execute "DROP TEXT SEARCH CONFIGURATION urbo_portuguese"
    execute "DROP EXTENSION unaccent"
  end
end
