class NormalizeAttachmentsTextData < ActiveRecord::Migration
  def change
    Attachment.find_each(batch_size: 10) do |attachment|
      attachment.text_data = Search.normalize(attachment.text_data)
      attachment.save!
    end
  end
end
