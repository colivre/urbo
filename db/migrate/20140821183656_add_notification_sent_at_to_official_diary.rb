class AddNotificationSentAtToOfficialDiary < ActiveRecord::Migration
  def change
    add_column :official_diaries, :notification_sent_at, :datetime
  end
end
