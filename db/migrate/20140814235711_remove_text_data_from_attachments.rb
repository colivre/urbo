class RemoveTextDataFromAttachments < ActiveRecord::Migration
  def up
    remove_column :attachments, :text_data
  end

  def down
    add_column :attachments, :text_data
  end
end
