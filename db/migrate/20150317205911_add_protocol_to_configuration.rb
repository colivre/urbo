class AddProtocolToConfiguration < ActiveRecord::Migration
  def change
    add_column :official_diary_configurations, :protocol, :string, default: 'http'
  end
end
