class AddCoverAndWallpaperToOfficialDiaryConfiguration < ActiveRecord::Migration
  def change
    add_column :official_diary_configurations, :cover, :binary
    add_column :official_diary_configurations, :wallpaper, :binary
  end
end
