class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.string :email
      t.boolean :enabled, default: true
      t.string :token

      t.timestamps
    end
  end
end
