class DropSearchView < ActiveRecord::Migration
  def up
    execute 'DROP VIEW IF EXISTS searches'
  end

  def down
    puts "Not really undoable"
  end
end
