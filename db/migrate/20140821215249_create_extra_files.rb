class CreateExtraFiles < ActiveRecord::Migration
  def change
    create_table :extra_files do |t|
      t.binary :data,         null: false
      t.string :filename,     null: false
      t.string :content_type, null: false

      t.integer :subject_id,  null: false

      t.timestamps
    end

    add_index :extra_files, :subject_id
  end
end
