class CreateSearchView < ActiveRecord::Migration
  def up
    execute <<-EOF
      CREATE VIEW searches AS

      SELECT
        official_diary_id,
        date,
        attachments.text_data as contents
      FROM official_diaries
      JOIN attachments ON (attachments.official_diary_id = official_diaries.id)

      UNION

      SELECT
        official_diary_id,
        date,
        contents
      FROM subjects

    EOF
  end

  def down
    execute 'DROP VIEW searches'
  end
end
