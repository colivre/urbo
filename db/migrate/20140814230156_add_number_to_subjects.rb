class AddNumberToSubjects < ActiveRecord::Migration
  def change
    add_column :subjects, :number, :integer
    execute 'UPDATE subjects SET number = (SELECT number from official_diaries where official_diaries.id = subjects.official_diary_id)'
  end
end
