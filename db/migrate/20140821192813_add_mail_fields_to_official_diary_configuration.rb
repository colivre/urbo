# encoding: UTF-8

class AddMailFieldsToOfficialDiaryConfiguration < ActiveRecord::Migration
  def change
    add_column :official_diary_configurations, :hostname, :string, default: 'localhost:3000'
    add_column :official_diary_configurations, :mail_from, :string, default: 'noreply@example.com'
    add_column :official_diary_configurations, :mail_subject, :string, default: 'Nova edição do diário oficial'
  end
end
