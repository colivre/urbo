class DropUploadFieldsFromAttachments < ActiveRecord::Migration
  def up
    remove_column :attachments, :original_filename
    remove_column :attachments, :content_type
  end

  def down
    add_column :attachments, :content_type, :string
    add_column :attachments, :original_filename, :string
  end
end
