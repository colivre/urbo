--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: unaccent; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS unaccent WITH SCHEMA public;


--
-- Name: EXTENSION unaccent; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION unaccent IS 'text search dictionary that removes accents';


SET search_path = public, pg_catalog;

--
-- Name: urbo_portuguese; Type: TEXT SEARCH CONFIGURATION; Schema: public; Owner: -
--

CREATE TEXT SEARCH CONFIGURATION urbo_portuguese (
    PARSER = pg_catalog."default" );

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR asciiword WITH unaccent, portuguese_stem;

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR word WITH unaccent, portuguese_stem;

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR numword WITH simple;

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR email WITH simple;

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR url WITH simple;

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR host WITH simple;

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR sfloat WITH simple;

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR version WITH simple;

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR hword_numpart WITH simple;

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR hword_part WITH portuguese_stem;

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR hword_asciipart WITH portuguese_stem;

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR numhword WITH simple;

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR asciihword WITH portuguese_stem;

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR hword WITH portuguese_stem;

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR url_path WITH simple;

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR file WITH simple;

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR "float" WITH simple;

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR "int" WITH simple;

ALTER TEXT SEARCH CONFIGURATION urbo_portuguese
    ADD MAPPING FOR uint WITH simple;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: attachments; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE attachments (
    id integer NOT NULL,
    official_diary_id integer,
    data bytea,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    date date
);


--
-- Name: attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE attachments_id_seq OWNED BY attachments.id;


--
-- Name: books; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE books (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    "position" integer DEFAULT 0
);


--
-- Name: books_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE books_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: books_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE books_id_seq OWNED BY books.id;


--
-- Name: departments; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE departments (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: departments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE departments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: departments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE departments_id_seq OWNED BY departments.id;


--
-- Name: extra_files; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE extra_files (
    id integer NOT NULL,
    data bytea NOT NULL,
    filename character varying(255) NOT NULL,
    content_type character varying(255) NOT NULL,
    subject_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    title character varying(255)
);


--
-- Name: extra_files_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE extra_files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: extra_files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE extra_files_id_seq OWNED BY extra_files.id;


--
-- Name: official_diaries; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE official_diaries (
    id integer NOT NULL,
    date date NOT NULL,
    number integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    publication_date timestamp without time zone,
    notification_sent_at timestamp without time zone
);


--
-- Name: official_diaries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE official_diaries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: official_diaries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE official_diaries_id_seq OWNED BY official_diaries.id;


--
-- Name: official_diary_configurations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE official_diary_configurations (
    id integer NOT NULL,
    number_of_columns integer DEFAULT 1,
    font_size integer DEFAULT 12,
    header text,
    footer text,
    preamble text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    cover bytea,
    wallpaper bytea,
    hostname character varying(255) DEFAULT 'localhost:3000'::character varying,
    mail_from character varying(255) DEFAULT 'noreply@example.com'::character varying,
    mail_subject character varying(255) DEFAULT 'Nova edição do diário oficial'::character varying,
    certificate text,
    protocol character varying(255) DEFAULT 'http'::character varying
);


--
-- Name: official_diary_configurations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE official_diary_configurations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: official_diary_configurations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE official_diary_configurations_id_seq OWNED BY official_diary_configurations.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: subjects; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE subjects (
    id integer NOT NULL,
    official_diary_id integer NOT NULL,
    book_id integer NOT NULL,
    date date NOT NULL,
    contents text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    page integer,
    normalized_contents text,
    department_id integer,
    title character varying(255),
    "position" integer DEFAULT 0,
    number integer
);


--
-- Name: subjects_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE subjects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subjects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE subjects_id_seq OWNED BY subjects.id;


--
-- Name: subscriptions; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE subscriptions (
    id integer NOT NULL,
    email character varying(255),
    enabled boolean DEFAULT false,
    token character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: subscriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE subscriptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: subscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE subscriptions_id_seq OWNED BY subscriptions.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    username character varying(255),
    email character varying(255),
    hashed_password character varying(255),
    salt character varying(255),
    preferences text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    role character varying(255) DEFAULT 'editor'::character varying,
    department_id integer
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY attachments ALTER COLUMN id SET DEFAULT nextval('attachments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY books ALTER COLUMN id SET DEFAULT nextval('books_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY departments ALTER COLUMN id SET DEFAULT nextval('departments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY extra_files ALTER COLUMN id SET DEFAULT nextval('extra_files_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY official_diaries ALTER COLUMN id SET DEFAULT nextval('official_diaries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY official_diary_configurations ALTER COLUMN id SET DEFAULT nextval('official_diary_configurations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY subjects ALTER COLUMN id SET DEFAULT nextval('subjects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY subscriptions ALTER COLUMN id SET DEFAULT nextval('subscriptions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY attachments
    ADD CONSTRAINT attachments_pkey PRIMARY KEY (id);


--
-- Name: books_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY books
    ADD CONSTRAINT books_pkey PRIMARY KEY (id);


--
-- Name: departments_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY departments
    ADD CONSTRAINT departments_pkey PRIMARY KEY (id);


--
-- Name: extra_files_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY extra_files
    ADD CONSTRAINT extra_files_pkey PRIMARY KEY (id);


--
-- Name: official_diaries_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY official_diaries
    ADD CONSTRAINT official_diaries_pkey PRIMARY KEY (id);


--
-- Name: official_diary_configurations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY official_diary_configurations
    ADD CONSTRAINT official_diary_configurations_pkey PRIMARY KEY (id);


--
-- Name: subjects_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY subjects
    ADD CONSTRAINT subjects_pkey PRIMARY KEY (id);


--
-- Name: subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY subscriptions
    ADD CONSTRAINT subscriptions_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_extra_files_on_subject_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_extra_files_on_subject_id ON extra_files USING btree (subject_id);


--
-- Name: subjects_normalized_contents; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX subjects_normalized_contents ON subjects USING gin (to_tsvector('urbo_portuguese'::regconfig, normalized_contents));


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- PostgreSQL database dump complete
--

INSERT INTO schema_migrations (version) VALUES ('20131012205249');

INSERT INTO schema_migrations (version) VALUES ('20131012205250');

INSERT INTO schema_migrations (version) VALUES ('20131012210250');

INSERT INTO schema_migrations (version) VALUES ('20131012210339');

INSERT INTO schema_migrations (version) VALUES ('20131109235720');

INSERT INTO schema_migrations (version) VALUES ('20131207210225');

INSERT INTO schema_migrations (version) VALUES ('20131207224214');

INSERT INTO schema_migrations (version) VALUES ('20140131102746');

INSERT INTO schema_migrations (version) VALUES ('20140131104533');

INSERT INTO schema_migrations (version) VALUES ('20140131110429');

INSERT INTO schema_migrations (version) VALUES ('20140131171543');

INSERT INTO schema_migrations (version) VALUES ('20140208192213');

INSERT INTO schema_migrations (version) VALUES ('20140208193521');

INSERT INTO schema_migrations (version) VALUES ('20140208210948');

INSERT INTO schema_migrations (version) VALUES ('20140208211828');

INSERT INTO schema_migrations (version) VALUES ('20140219121322');

INSERT INTO schema_migrations (version) VALUES ('20140617094117');

INSERT INTO schema_migrations (version) VALUES ('20140711020814');

INSERT INTO schema_migrations (version) VALUES ('20140729153655');

INSERT INTO schema_migrations (version) VALUES ('20140801184040');

INSERT INTO schema_migrations (version) VALUES ('20140814230156');

INSERT INTO schema_migrations (version) VALUES ('20140814235711');

INSERT INTO schema_migrations (version) VALUES ('20140815004157');

INSERT INTO schema_migrations (version) VALUES ('20140819224125');

INSERT INTO schema_migrations (version) VALUES ('20140821173227');

INSERT INTO schema_migrations (version) VALUES ('20140821183656');

INSERT INTO schema_migrations (version) VALUES ('20140821192813');

INSERT INTO schema_migrations (version) VALUES ('20140821215249');

INSERT INTO schema_migrations (version) VALUES ('20140903131418');

INSERT INTO schema_migrations (version) VALUES ('20140910171437');

INSERT INTO schema_migrations (version) VALUES ('20150317163420');

INSERT INTO schema_migrations (version) VALUES ('20150317205911');